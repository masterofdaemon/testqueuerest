package main

import (
	"errors"
	"fmt"
	"net/http"
)

type Obj struct {
	Key string
	Value string
}
// QueueData main storage queue
type QueueData struct {
	data []Obj
}

var errorMess = map[string]string{
	"valNF": "value is not found",
}


func searchObj(data []Obj, key string) (int, error){
	for i, val := range data {
		if val.Key == key {
			return i, nil
		}
	}
	return 0, errors.New(errorMess["valNF"])
}

func (qd *QueueData) pop(index int)  {
	copy(qd.data[index:], qd.data[index+1:])
	qd.data[len(qd.data)-1] = Obj{}
	qd.data = qd.data[:len(qd.data)-1]
}

func (qd *QueueData)routeHandler(w http.ResponseWriter, req *http.Request) {
		queryKey := req.URL.Path[1:]
	switch req.Method {
	case "PUT":
		val := req.URL.Query().Get("v")
		if val == "" || queryKey == "" {
			http.Error(w,"", http.StatusBadRequest)
			return
		}
		qd.data = append(qd.data, Obj{Value: val, Key: queryKey})
	case "GET":

		index, errValNF := searchObj(qd.data, queryKey)
		if errValNF != nil {
			http.Error(w,"", http.StatusNotFound)
			return
		}
		value := qd.data[index].Value
		go qd.pop(index)
		fmt.Fprintf(w, "%v\n", value)

	}
}



func (qd *QueueData) init() {
	http.HandleFunc("/", qd.routeHandler)

	err := http.ListenAndServe(":8090", nil)
	if err != nil {
		return
	}
}
func main()  {
	qd := QueueData{}
	qd.init()
}